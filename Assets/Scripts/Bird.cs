using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class Bird : MonoBehaviour
{
    
    public GameObject Parent;
    public Rigidbody2D RigidBody;
    public CircleCollider2D Collider;
    protected float _destroyDelay = 2f;

    protected BirdState _state;
    float _minVelocity = 0.05f;
    bool _flagDestroy = false;

    public UnityAction OnBirdDestroyed = delegate{};
    public UnityAction<Bird> OnBirdShot = delegate{};

    public BirdState State {get {return _state; }}

    void OnDestroy() {
        if(_state == BirdState.Thrown || _state == BirdState.HitSomething)
            OnBirdDestroyed();
    }

    void OnCollisionEnter2D(Collision2D collision) {
        _state = BirdState.HitSomething;
    }

    IEnumerator DestroyAfter(float second) {
        yield return new WaitForSeconds(second);
        Destroy(gameObject);
    }

    public void MoveTo(Vector2 target, GameObject parent) {
        gameObject.transform.SetParent(parent.transform);
        gameObject.transform.position = target;
    }

    public void Shoot(Vector2 velocity, float distance, float speed) {
        Collider.enabled = true;
        RigidBody.bodyType = RigidbodyType2D.Dynamic;
        RigidBody.velocity = velocity*speed*distance;

        OnBirdShot(this);
    }

    public virtual void OnTap() {
        return;
    }

    // Start is called before the first frame update
    void Start()
    {
        RigidBody.bodyType = RigidbodyType2D.Kinematic;
        Collider.enabled = false;
        _state = BirdState.Idle;
    }

    void FixedUpdate() {
        if(_state == BirdState.Idle && RigidBody.velocity.sqrMagnitude >= _minVelocity) {
            _state = BirdState.Thrown;
        }

        if (_state == BirdState.Thrown || _state == BirdState.HitSomething 
            && RigidBody.velocity.sqrMagnitude < _minVelocity && !_flagDestroy) {
            //Destroy object after 2 seconds
            //If the speed is lower than min velocity
            _flagDestroy = true;
            StartCoroutine(DestroyAfter(_destroyDelay));
        }
    }
}

public enum BirdState {Idle, Thrown, HitSomething}
