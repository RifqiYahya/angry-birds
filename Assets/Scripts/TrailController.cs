using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailController : MonoBehaviour
{
    public GameObject Trail;
    public Bird TargetBird;

    List<GameObject> _trails = new List<GameObject>();

    public void SetBird(Bird bird) {
        TargetBird = bird;

        foreach(GameObject trail in _trails) {
            Destroy(trail);
        }

        _trails.Clear();
    }

    public IEnumerator SpawnTrail() {
        _trails.Add(Instantiate(Trail, TargetBird.transform.position, Quaternion.identity));
        yield return new WaitForSeconds(0.1f);
        if(TargetBird != null && TargetBird.State != BirdState.HitSomething) {
            StartCoroutine(SpawnTrail());
        }
    }
}
