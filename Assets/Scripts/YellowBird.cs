using UnityEngine;

public class YellowBird : Bird
{
    [SerializeField] float _boostForce = 100;
    bool _boosted = false;

    public void Boost() {
        if (State == BirdState.Thrown && !_boosted) {
            RigidBody.AddForce(RigidBody.velocity*_boostForce);
            _boosted = true;
        }
    }

    public override void OnTap()
    {
        Boost();
    }
}
