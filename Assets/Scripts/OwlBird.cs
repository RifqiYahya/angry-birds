using UnityEngine;
using System.Collections;

public class OwlBird : Bird
{
    [SerializeField] float _explosionRadius = 0.5f;
    [SerializeField] float _explosionPower = 50f;
    [SerializeField] float _explosionDelay = 2f;

    public OwlBird() {
        _destroyDelay = 5f;
    }

    IEnumerator Explode() {
        yield return new WaitForSeconds(_explosionDelay);

        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, _explosionRadius);
        float distance;
        Vector2 dir;

        foreach(Collider2D collider in colliders) {
            
            distance = Vector2.Distance(collider.transform.position, transform.position);
            dir = (collider.transform.position - transform.position).normalized;

            if(collider.gameObject.tag == "Obstacle" || collider.gameObject.tag == "Enemy") {
                collider.gameObject.GetComponent<Rigidbody2D>().AddForce(dir/distance*_explosionPower);
            }
        }

        Destroy(gameObject);
    }

    void OnCollisionEnter2D(Collision2D collision) {
        _state = BirdState.HitSomething;
        StartCoroutine(Explode());
    }
}
