using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public SlingShooter SlingShooter;
    public TrailController TrailController;
    public List<Bird> Birds;
    public List<Enemy> Enemies;
    public BoxCollider2D TapCollider;
    Bird _shotBird;

    public GameObject GameOverPanel;
    public Text EndgameText;

    bool _isGameEnded = false;

    void ChangeBird() {
        TapCollider.enabled = false;

        if(_isGameEnded) return;

        Birds.RemoveAt(0);
        if (Birds.Count > 0) {
            SlingShooter.InitiateBird(Birds[0]);
            _shotBird = Birds[0];
        } else if (Enemies.Count > 0) {
            StartCoroutine(CheckWin());
        }
    }

    public void CheckGameEnd(GameObject destroyedEnemy) {
        foreach(Enemy enemy in Enemies) {
            if(enemy.gameObject == destroyedEnemy) {
                Enemies.Remove(enemy);
                break;
            }
        }

        if (Enemies.Count == 0) {
            _isGameEnded = true;
            EndGame(true);
        }
    }

    public void AssignTrail(Bird bird) {
        TrailController.SetBird(bird);
        StartCoroutine(TrailController.SpawnTrail());
        TapCollider.enabled = true;
    }

    void OnMouseUp() {
        if(_shotBird != null) {
            _shotBird.OnTap();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        foreach(Bird bird in Birds) {
            bird.OnBirdDestroyed += ChangeBird;
            bird.OnBirdShot += AssignTrail;
        }

        foreach(Enemy enemy in Enemies) {
            enemy.OnEnemyDestroyed += CheckGameEnd;
        }

        TapCollider.enabled = false;
        SlingShooter.InitiateBird(Birds[0]);
        _shotBird = Birds[0];
    }

    public void EndGame (bool isWin) {
        if (isWin) {
            EndgameText.text = "You Win!";
        } else {
            EndgameText.text = "You Lose!";
        }

        GameOverPanel.SetActive(true);
    }

    IEnumerator CheckWin() {
        yield return new WaitForSeconds(3f);
        if (Enemies.Count > 0) {
            EndGame(false);
        }
    }
}
